<?php

class EditController extends BaseController
{
    private $varArray;
    private $verify_service;
    private $user_service;

    public function __construct()
    {
        parent::__construct();
        $this->verify_service = new VerifyService();
        $this->user_service = new UserService();
        $this->layout_service = new LayoutService();
    }

    public function startController($params){
        if ($this->verify_service->check_if_logged()) {
          $this->array_push_assoc($this->varArray, "baseURI", $this->baseURI);
          $this->array_push_assoc($this->varArray, "allUsers", $this->user_service->get_all_users());
          $this->view->render('edit', $this->varArray);
        }
        else {
          //neautorizovan;
          http_response_code(401);
        }

    }

    public function call_delete_user_by_id($params) {
      if ($this->verify_service->check_if_logged()) {
        $this->user_service->delete_user($params[0]);
        $this->layout_service->generate_user_list_view();
      }
      else {
        http_response_code(401);
      }
    }

    public function call_update_user($params) {
      if ($this->verify_service->check_if_logged()) {
          $user = $this->user_service->get_user_by_id($params[0]);

          $nickname = $user['nickname'];
          $password = $user['password'];

          if (isset($_POST['nickname'])){
            if ($_POST['nickname'] !== ''){
              $nickname = preg_replace('/\s+/', '', $_POST['nickname']);
              $nickname = strip_tags($nickname);
            }
          }

          if (isset($_POST['password'])){
            if ($_POST['password'] !== ''){
              $password = strip_tags($_POST['password']);
              $password = preg_replace('/\s+/', '', $password);
              $password = password_hash($password, PASSWORD_DEFAULT);
            }
          }

          $this->user_service->update_user($nickname, $password, $user['id']);
          $this->layout_service->generate_user_list_view();
        }
        else {
          http_response_code(401);
        }
    }

}

<?php


class HomepageController extends BaseController
{
    private $varArray;
    private $verify_service;


    public function __construct()
    {
        parent::__construct();
        $this->verify_service = new VerifyService();
    }

    public function startController($params){
        $this->array_push_assoc($this->varArray, "baseURI", $this->baseURI);
        $this->array_push_assoc($this->varArray, "loginInfo", "");

        if ($this->verify_service->check_if_logged()) {
          $this->view->render('homepage', $this->varArray);
        }
        else {
          //neautorizovan;
          http_response_code(401);
        }

    }

}

<?php

class AddController extends BaseController
{
    private $varArray;
    private $verify_service;
    private $user_service;

    public function __construct()
    {
        parent::__construct();
        $this->verify_service = new VerifyService();
        $this->user_service = new UserService();
    }



    public function startController($params){
      if ($this->verify_service->check_if_logged()) {
        $this->array_push_assoc($this->varArray, "baseURI", $this->baseURI);
        $this->view->render('add', $this->varArray);
      }
      else {
        //neautorizovan;
        http_response_code(401);
      }

    }

    public function call_add_user() {
        $nickname = $_POST['nickname'];
        $password = $_POST['password'];

        if ($this->verify_service->check_if_logged()) {
          if ($password !== '' || $nickname !== ''){
            $password = strip_tags($_POST['password']);
            $password = preg_replace('/\s+/', '', $password);
            $password = password_hash($password, PASSWORD_DEFAULT);

            $nickname = preg_replace('/\s+/', '', $_POST['nickname']);
            $nickname = strip_tags($nickname);
            $this->user_service->add_user($nickname, $password);
          }
          else http_response_code(500);
        }
        else http_response_code(401);
    }

}

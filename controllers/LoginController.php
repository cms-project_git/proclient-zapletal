<?php

class LoginController extends BaseController
{
    private $varArray;
    private $login_service;
    private $verify_service;

    public function __construct()
    {
        parent::__construct();
        $this->login_service = new LoginService();
        $this->verify_service = new VerifyService();

    }



    public function startController($params){
        if ($this->verify_service->check_if_logged()) {
          header('Location: ' . $this->baseURI . "homepage");
        }
        else {
          $this->array_push_assoc($this->varArray, "baseURI", $this->baseURI);
          $this->array_push_assoc($this->varArray, "loginInfo", "");
          $this->view->render('login', $this->varArray);
        }
    }

    public function call_do_login() {
        $nick = $_POST['nickname'];
        $password = $_POST['password'];

        $valid = $this->login_service->check_credentials($nick, $password);

        if ($valid) {
          $this->login_service->login_user($valid['id']);
          http_response_code(200);
        }
        else {
          http_response_code(404);
        }

    }

    public function call_do_logout() {
        $this->login_service->logout_user();
    }
}

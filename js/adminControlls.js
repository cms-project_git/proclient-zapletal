var baseURI = "http://localhost/infs/";

$(document).ready(function() {
    $('#loginBtn').click(function(e) {
      $.ajax({
        type: "POST",
        url: "login/call_do_login",
        data: $('#loginForm').serialize(),
        success: function (data) {
          $(location).attr('href', baseURI + "homepage");
        },
        error: function(jqXHR, textStatus, errorThrown) {
          $('.bad-credentials').show().delay(4000).fadeOut();
        }
      });
    });

    $('#logout').click(function(e) {
      $.ajax({
        type: "GET",
        url:  "login/call_do_logout",
        success: function (data) {
          $(location).attr('href', baseURI + "login");
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
      });
    });

    // z duvodu dynamickyho contentu musim pouzit document onclick
    $(document).on('click','.delete', function(e){
      e.preventDefault();
      var userId = $(this).data('userid');


      if (confirm("Opravdu chcete smazat tohoto uživatele?")){
        $.ajax({
          type: "GET",
          url: "edit/call_delete_user_by_id/" + userId,
          success: function (data) {
            $('.succ').show().delay(4000).fadeOut();
            $('.users-list-holder').html(data);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
          }
        });
      }


    });


    $("#addUserBtn").click(function(e) {
      if (checkAddingForm()){
        e.preventDefault();
        $.ajax({
          type: "POST",
          url: "add/call_add_user",
          data: $('#addUserForm').serialize(),
          success: function (data) {
            $('.success-added').show().delay(4000).fadeOut();
            $("#addUserForm").trigger('reset');
          },
          error: function(jqXHR, textStatus, errorThrown) {
            $('.fail-not-added').show().delay(4000).fadeOut();
          }
        });
      }


    });

    // z duvodu dynamickyho contentu musim pouzit document onclick
    $(document).on('click','.edit-btn', function(e){
      e.preventDefault();
      var data = $(this).data("userid");
      $('.item-' + data).toggle();

    });

    function checkAddingForm() {
      var flag = true;
      var indexArray = new Array("nickname", "password");

      for (var i = 0; i < indexArray.length; i++){
          if (!document.getElementById(indexArray[i]).checkValidity()){
              flag = false;
          }
      }
      return flag;
    }

    // z duvodu dynamickyho contentu musim pouzit document onclick
    $(document).on('click','.edit-send-btn', function(e){
      e.preventDefault();
      var userId = $(this).data('targetid');

      $.ajax({
        type: "POST",
        url: "edit/call_update_user/" + userId,
        data: $("#editUserForm-" + userId).serialize(),
        success: function (data) {
          $('.succ').show().delay(4000).fadeOut();
          $('.users-list-holder').html(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          $('.warning').show().delay(4000).fadeOut();
        }
      });
    });


});

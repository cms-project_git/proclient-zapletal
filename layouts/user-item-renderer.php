<tr>
  <td><?php echo $user['nickname'];?>
    <?php
      if ($user['logged'] == 1){
        echo "<span class='login-info'> přihlášen </span>";
      }
      else echo "<span class='login-info'> odhlášen </span>";
    ?>
  </td>
  <td>
    <a href="" class="edit-btn" data-userid="<?php echo $user['id'];?>">
      <i class="action-icon fa fas fa-edit"></i>
    </a>
    <?php if ($user['logged'] == 0) {?>
    <a href="" class="delete" data-userid="<?php echo $user['id'];?>">
      <i class="action-icon danger fa fas fa-trash-alt"></i>
    </a>
    <?php }?>
    <div class="edit-form hidden item-<?php echo $user['id'];?>">
      <form method="POST" id="editUserForm-<?php echo $user['id'];?>" class="inner-padding">
          <div class="form-group">
            <label for="nickname">Nickname</label>
            <input type="nickname" name="nickname" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="password">Heslo</label>
            <input type="password" name="password" class="form-control" required>
          </div>
          <button type="submit" data-targetid="<?php echo $user['id'];?>" class="edit-send-btn btn btn-default">Změnit</button>
      </form>
    </div>
  </td>
</tr>

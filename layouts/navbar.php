<nav>
  <ul>
    <a href="/infs/add">
      <li><i class="fas fa-user-plus"></i> přidat</li>
    </a>
    <a href="/infs/edit">
      <li><i class="fas fa-tasks"></i> spravovat</li>
    </a>
    <a href="/infs/login/call_do_logout" id="logout">
      <li><i class="fas fa-sign-out-alt"></i> odhlásit se</li>
    </a>
  </ul>
</nav>

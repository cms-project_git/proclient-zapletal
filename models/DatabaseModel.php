<?php
/**
 * Created by PhpStorm.
 * User: Patrik
 * Date: 24.03.2018
 * Time: 11:02
 */

define ('USER_NAME', 'root');
define ('PASSWORD', '');
define ('HOST_NAME', 'localhost');
define ('DATABASE_NAME', 'blog');

class DatabaseModel
{

    private $database;

    public function __construct($hostname = HOST_NAME, $dbname = DATABASE_NAME, $username = USER_NAME, $password = PASSWORD)
    {
        try {
            $this->database = new PDO('mysql:host=localhost;dbname=administration;charset=utf8;', USER_NAME, PASSWORD);
            $this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->database->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
        } catch (PDOException $e) {
            print "Error" . $e->getMessage() . "<br/>";
        }
    }

    public function add_user($nickname, $password) {
      $params = array($nickname, $password);
      $pdo = $this->database->prepare("INSERT INTO user (nickname, password, logged) VALUES (?, ?, 0)");

      $result = $pdo->execute($params);
      echo $result;

      return $result;
    }

    public function find_user($nickname) {
      $params = array($nickname);
      $pdo = $this->database->prepare("SELECT * FROM administration.user WHERE nickname = ?");
      $pdo->execute($params);

      $user = $pdo->fetch();

      return $user;
    }

    public function find_user_by_id($id) {
      $params = array($id);
      $pdo = $this->database->prepare("SELECT * FROM administration.user WHERE id = ?");
      $pdo->execute($params);

      $user = $pdo->fetch();

      return $user;
    }

    public function get_all_users() {
      $pdo = $this->database->prepare("SELECT * FROM administration.user");
      $pdo->execute();

      $users = $pdo->fetchAll();

      return $users;
    }

    public function delete_user($id) {
      $pdo = $this->database->prepare("DELETE FROM administration.user WHERE id = ?");
      $params = array($id);

      $result = $pdo->execute($params);

      return $result;
    }

    public function set_as_logged($id) {
      $pdo = $this->database->prepare("UPDATE administration.user SET logged = 1 WHERE id = ?");

      $params = array($id);

      $result = $pdo->execute($params);

      return $result;


    }

    public function set_as_logged_out($id) {
      $pdo = $this->database->prepare("UPDATE administration.user SET logged = 0 WHERE id = ?");

      $params = array($id);

      $result = $pdo->execute($params);

      return $result;
    }

    public function update_user($nickname, $password, $id) {
      $pdo = $this->database->prepare("UPDATE administration.user SET nickname = ? WHERE id = ?");

      $params = array($nickname, $id);

      $result_nickname = $pdo->execute($params);

      $pdo = $this->database->prepare("UPDATE administration.user SET password = ? WHERE id = ?");

      $params = array($password, $id);

      $result_password = $pdo->execute($params);

      return $result_nickname && $result_password;
    }

}

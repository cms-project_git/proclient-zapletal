<?php

class UserService {

  private $database_model;
  private $verify_service;

  public function __construct() {
      $this->database_model = new DatabaseModel();
      $this->verify_service = new VerifyService();
  }

  public function get_all_users() {
    $users = $this->database_model->get_all_users();

    return $users;
  }

  public function delete_user($id) {
      try {
          $user = $this->database_model->find_user_by_id($id);

          if ($user['logged'] == 0) {
            $this->database_model->delete_user($id);
          }
          else {
            http_response_code(500);
          }
      }
      catch (PDOException $ex) {
        http_response_code(500);
      }
  }

  public function add_user($nickname, $password) {
      try {
        $result = $this->database_model->add_user($nickname, $password);
        http_response_code(200);
      }
      catch (PDOException $exception) {
        http_response_code(500);
      }
  }

  public function update_user($nickname, $password, $id) {
      try {
        $result = $this->database_model->update_user($nickname, $password, $id);
        http_response_code(200);
      }
      catch (PDOException $exception) {
        http_response_code(500);
      }
  }

  public function get_user_by_id($id) {
    return $this->database_model->find_user_by_id($id);
  }
}

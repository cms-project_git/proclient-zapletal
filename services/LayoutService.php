<?php

class LayoutService {

  private $database_model;
  private $user_service;

  public function __construct() {
      $this->database_model = new DatabaseModel();
      $this->user_service = new UserService();
  }

  public function generate_user_list_view() {
      $users = $this->user_service->get_all_users();
      $output = "";

      foreach ($users as $user) {
        ob_start();
        require('layouts/user-item-renderer.php');
        $output .= ob_get_clean();
      }

      echo $output;
  }

}

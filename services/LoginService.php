<?php

class LoginService {

  private $database_model;

  public function __construct() {
      $this->database_model = new DatabaseModel();
  }

  public function check_credentials($nickname, $password) {
      $user = $this->database_model->find_user($nickname);

      if ($user && password_verify($password, $user['password'])) {
        return $user;
      }

      return false;
  }

  public function login_user($id) {
    $_SESSION['logged'] = true;
    $_SESSION['id'] = $id;
    $this->database_model->set_as_logged($id);
  }

  public function logout_user() {
    $_SESSION['logged'] = false;
    $id = $_SESSION['id'];
    unset($_SESSION['id']);
    $this->database_model->set_as_logged_out($id);
  }
}

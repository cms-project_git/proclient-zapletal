<?php

require 'libs/Bootstrap.php';
require 'libs/BaseController.php';
require 'libs/View.php';
require 'models/DatabaseModel.php';
require 'services/LoginService.php';
require 'services/VerifyService.php';
require 'services/UserService.php';
require 'services/LayoutService.php';

session_start();

$app = new Bootstrap();

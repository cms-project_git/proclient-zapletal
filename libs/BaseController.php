<?php

class BaseController
{

    public $database;
    public $baseURI = "http://localhost/infs/";

    public function __construct()
    {
        $this->database = new DatabaseModel();
        $this->view = new View();
    }

    function array_push_assoc(&$array, $key, $value)
    {
        $array[$key] = $value;
        return $array;
    }

}

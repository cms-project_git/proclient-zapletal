<?php

class Bootstrap
{
    public function __construct()
    {
        //vyzvednu z pole request
        $url = $_GET['request'];
        //rozparseruju
        $url = explode('/', $url);
        $this->if_exists_require($url);

    }

    /* Funkce, ktera k pozadavku ziska nazev tridy controlleru
       Vzdy ve tvaru urlController.php */
    private function make_controller_class_name($url)
    {
        $name = ucfirst($url);
        $name = basename($name, ".php");
        $name = $name . "Controller";
        return $name;
    }

    /* Ziskam ostatni parametry z adresy */
    private function get_other_parametres($url)
    {
        $params = [];
        foreach ($url as $single) {
            //ten prvni uz nepotrebuji
            if (!($single === reset($url))) {
                array_push($params, $single);
            }
        }
        return $params;
    }

    private function get_other_parametres_subfolder($url)
    {
        for ($i = 0; $i < sizeof($url); $i++) {
            $params = [];
            if ($i != 0 or $i != 1) {
                array_push($params, $url[$i]);
            }
        }
        return $params;
    }

    /* Pokud dany soubor existuje tak si ho vyzadam a najdu jeho controller */
    private function if_exists_require($url)
    {
        $class_name = $this->make_controller_class_name($url[0]);
        $file = "controllers/" . $class_name . '.php';
        $params = $this->get_other_parametres($url);

        if (file_exists($file)) {
            require $file;
            $methods = get_class_methods($class_name);
            if (empty($params)) $params = array('none');

            if (in_array($params[0], $methods)) {
                if ($this->check_if_callable($params[0])) {
                    $controller = new $class_name();
                    $funParams = $this->get_other_parametres($params);
                    $controller->{$params[0]}($funParams);
                } else echo "404";
            } else {
                $controller = new $class_name($params);
                $controller->{'startController'}($params);
            }

        } else {
            echo "Controller $file neexistuje";
            return false;
        }
    }

    //pokud je funkce call_[nazev_funkce} tak lze volat pres URL
    private function check_if_callable($string)
    {
        if (substr($string, 0, 5) === "call_") return true;
        else return false;
    }


}

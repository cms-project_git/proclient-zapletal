<?php

class View
{
    function __construct()
    {
    }

    public function render($template_name, $input_data)
    {
        if (is_array($input_data)) {
            extract($input_data, EXTR_OVERWRITE);
        }
        require 'views/' . $template_name . '.php';
    }
}

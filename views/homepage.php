<!DOCTYPE html>
<html>
<head>
    <!-- KODOVANI !-->
    <meta charset="UTF-8">
    <!-- CSS !-->
    <link rel="stylesheet" type="text/css" href= <?php echo "'". $baseURI . "/css/styles.css'>"?>
    <!-- FONTS !-->
    <link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- BOOTSTRAP !-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- FONT AWESOME ICONS !-->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <!-- JQUERY !-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo $baseURI . 'js/adminControlls.js' ?>"></script>
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <title>
       Homepage
    </title>
</head>
<body>
    <aside>
      <?php require('layouts/navbar.php');?>
    </aside>
    <main>
      <h1> Homepage </h1>
      <p>
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nunc tincidunt ante vitae massa. Aliquam erat volutpat. Maecenas libero. Nunc tincidunt ante vitae massa. Aliquam ornare wisi eu metus. Praesent dapibus. Aliquam erat volutpat. Donec vitae arcu. Integer lacinia. Suspendisse sagittis ultrices augue. In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Etiam bibendum elit eget erat. Nullam rhoncus aliquam metus. Nunc dapibus tortor vel mi dapibus sollicitudin.
      </p>
      <p>
        Donec quis nibh at felis congue commodo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Mauris tincidunt sem sed arcu. Integer tempor. Aenean id metus id velit ullamcorper pulvinar. Duis viverra diam non justo. Etiam dictum tincidunt diam. Aliquam ornare wisi eu metus. Etiam bibendum elit eget erat. Sed convallis magna eu sem. Cras pede libero, dapibus nec, pretium sit amet, tempor
      </p>
      <p>
        quis. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Nulla non arcu lacinia neque faucibus fringilla. Pellentesque sapien. Aliquam ante. Nullam at arcu a est sollicitudin euismod. Curabitur bibendum justo non orci. Nullam sapien sem, ornare ac, nonummy non, lobortis a enim.
      </p>
    </main>
</body>
</html>

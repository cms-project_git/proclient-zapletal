<!DOCTYPE html>
<html>
<head>
    <!-- KODOVANI !-->
    <meta charset="UTF-8">
    <!-- CSS !-->
    <link rel="stylesheet" type="text/css" href= <?php echo "'". $baseURI . "/css/styles.css'>"?>
    <!-- FONTS !-->
    <link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- BOOTSTRAP !-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- FONT AWESOME ICONS !-->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <!-- JQUERY !-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo $baseURI . 'js/adminControlls.js' ?>"></script>
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <title>
        Login
    </title>
</head>
<body class="body-background">
  <div class="wrapper-flex">
    <form class="form-horizontal" method="POST" id="loginForm">
      <div class="form-group">
        <label class="control-label col-sm-2" for="nickname">Nickname:</label>
        <input type="text" class="form-control" id="nickname" name="nickname">
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="password">Heslo:</label>
        <input type="password" class="form-control" id="password" name="password">
      </div>
      <button type="button" id="loginBtn" class="btn center-block float-right">přihlásit se</button>
    </form>
    <div class="bad-credentials hidden disclaimer red fixed-on-top"> Neplatné přihlašovací údaje.</div>
  </div>

</body>
</html>

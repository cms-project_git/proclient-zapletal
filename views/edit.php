<!DOCTYPE html>
<html>
<head>
    <!-- KODOVANI !-->
    <meta charset="UTF-8">
    <!-- CSS !-->
    <link rel="stylesheet" type="text/css" href= <?php echo "'". $baseURI . "/css/styles.css'>"?>
    <!-- FONTS !-->
    <link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- BOOTSTRAP !-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- FONT AWESOME ICONS !-->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <!-- JQUERY !-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo $baseURI . 'js/adminControlls.js' ?>"></script>
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <title>
       Spravovat
    </title>
</head>
<body>
  <div class="wrapper">
    <aside>
      <?php require('layouts/navbar.php');?>
    </aside>
    <main>
      <h1>Spravovat databázi uživatelů </h1>
        <ul>
          <li> smazat lze pouze nepřihlášený uživatel </li>
          <li> prázdné pole při změně údajů znamená zachování původní hodnoty </li>
        </ul>

      <div class="disclaimer warning hidden red fixed-on-top">Změna neproběhla, uživatel se stejným jménem existuje.</div>
      <div class="disclaimer succ hidden green fixed-on-top">Úspešně změněno.</div>
      <table class="table table-striped ">
        <thead>
          <tr>
            <th>Nickname</th>
            <th>Akce</th>
          </tr>
        </thead>
        <tbody class="users-list-holder">
          <?php foreach ($allUsers as $user) {
            require('layouts/user-item-renderer.php');
          }?>
        </tbody>
      </table>
    </main>
  </div>
</body>
</html>
